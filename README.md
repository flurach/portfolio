# My Portfolio
My Portfolio Site v3
<img src="readme.png">


## How to start developing
All you need is

- yarn
- node

Firstly, install dependencies with `yarn`. Then, you can do:
``` sh
$ yarn dev
```
Which starts a `nodemon` session.

Alternatively, you can do `yarn start` to use `node` instead of `nodemon`.